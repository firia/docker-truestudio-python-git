From kamilcuk/docker-truestudio

Maintainer Kamil Cukrowski <kamilcukrowski@gmail.com>

# install packages
RUN set -xueo pipefail && \
  echo 'Install python2 python2-pip git' && \
  pacman --noconfirm -Sy git && \
  pacman --noconfirm -Sv --force libnsl && \
  pacman --noconfirm -Sy python2 python2-pip && \
  pip2 install intelhex && \
  ln -sf /usr/bin/python2.7 /usr/bin/python && \
  echo '- SUCCESS install ------------------'
