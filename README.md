# docker-truestudio-python-git
  
Archlinux docker with installed TrueStudio from Atollic for headless builds.
Added python2.7 and git.  

# license
  
My dockerfile is under MIT license (forked from https://github.com/Kamilcuk/docker-truestudio.git)
Truestudio from Atollic is under their license.  
